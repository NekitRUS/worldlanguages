VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} frmEdit 
   Caption         =   "�������������� ����"
   ClientHeight    =   5835
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   9765
   OleObjectBlob   =   "frmEdit.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "frmEdit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Path, Continent  As String
Public DataBase As Workbook
Public DataSheet As Worksheet
Dim list As New Collection
Dim governmentList As New Collection
Dim iterator As Integer


Private Sub cboCountry_Change()
  
    On Error GoTo Exception
    
    lblError.Visible = False
    If cboCountry.ListIndex = -1 And iterator = -1 Then
        Exit Sub
    End If
    If cboCountry.ListIndex = -1 Or cboCountry.Value = "" Then
        iterator = -1
        txtCapital.Value = ""
        txtArea.Value = ""
        txtLanguage.Value = ""
        txtMoney.Value = ""
        txtPopulation.Value = ""
        cboGovernment.Value = ""
        cmdDelete.Enabled = False
        cmdEdit.Caption = "��������"
    Else
        iterator = cboCountry.ListIndex + 1
        txtCapital.Value = list(iterator).Capital
        txtArea.Value = CStr(list(iterator).Area)
        txtLanguage.Value = list(iterator).Language
        txtMoney.Value = list(iterator).Money
        txtPopulation.Value = CStr(list(iterator).Population)
        cboGovernment.Value = list(iterator).Government
        cmdDelete.Enabled = True
        cmdEdit.Caption = "�������������"
    End If
    
    Exit Sub
    
Exception:
    MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbCritical + vbOKOnly, "����� ����"
    End
 
End Sub

Private Sub cmdCancel_Click()
  
    On Error GoTo Exception
    
    End
        
    Exit Sub
    
Exception:
    MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbCritical + vbOKOnly, "����� ����"
    End

End Sub

Private Sub cmdDelete_Click()
  
    On Error GoTo Exception
    
    lblError.Visible = False
    list.Remove iterator
    iterator = iterator - 1
    DataSheet.Rows(iterator + 2).Delete Shift:=xlUp
    DataSheet.Range(Cells(1, 1), Cells(1 + list.count, 7)).Columns.AutoFit
    DataBase.Save
    If list.count > 0 Then
        Call RefillGovernmentList
        If iterator = 0 Then
            cboCountry.RemoveItem iterator
            cboCountry.Value = ""
        Else
            cboCountry.ListIndex = iterator - 1
            cboCountry.RemoveItem iterator
        End If
    Else
        cboGovernment.Clear
        cboCountry.Clear
    End If
            
    Exit Sub
    
Exception:
    MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbCritical + vbOKOnly, "����� ����"
    End
 
End Sub

Private Sub cmdDelete_Exit(ByVal Cancel As MSForms.ReturnBoolean)
  
    On Error GoTo Exception
    
    lblError.Visible = False
        
    Exit Sub
    
Exception:
    MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbCritical + vbOKOnly, "����� ����"
    End

End Sub

Private Sub cmdEdit_Click()
  
    On Error GoTo Exception
    
    lblError.Visible = False
    If CheckFields = False Then
        With lblError
            .Caption = "��������� ������������ �����! ����� ��� ���� �������� ������������� ��� ����������!"
            .Visible = True
            .ForeColor = &HFF&
        End With
        Exit Sub
    End If
    
    If cmdEdit.Caption = "�������������" Then
        With list(iterator)
            .Capital = Trim(txtCapital.Value)
            .Area = CDbl(Trim(Replace(txtArea.Value, ".", ",")))
            .Language = Trim(txtLanguage.Value)
            .Money = Trim(txtMoney.Value)
            .Population = CLng(Trim(txtPopulation.Value))
            .Government = Trim(cboGovernment.Value)
        End With
        With DataSheet
            .Cells(iterator + 1, 2).Value = list(iterator).Capital
            .Cells(iterator + 1, 3).Value = list(iterator).Area
            .Cells(iterator + 1, 4).Value = list(iterator).Language
            .Cells(iterator + 1, 5).Value = list(iterator).Money
            .Cells(iterator + 1, 6).Value = list(iterator).Population
            .Cells(iterator + 1, 7).Value = list(iterator).Government
            .Range(Cells(1, 1), Cells(1 + list.count, 7)).Columns.AutoFit
        End With
        DataBase.Save
        Call RefillGovernmentList
        cboGovernment.Value = list(iterator).Government
        With lblError
            .Visible = True
            .Caption = "������ ������� ���������������!"
            .ForeColor = &H8000&
        End With
    Else
        Dim temp As Entry
        Set temp = New Entry
        With temp
            .Country = Trim(cboCountry.Value)
            .Capital = Trim(txtCapital.Value)
            .Area = CDbl(Trim(Replace(txtArea.Value, ".", ",")))
            .Language = Trim(txtLanguage.Value)
            .Money = Trim(txtMoney.Value)
            .Population = CLng(Trim(txtPopulation.Value))
            .Government = Trim(cboGovernment.Value)
        End With
        list.Add Item:=temp, Key:=temp.Country
        With DataSheet
            .Cells(list.count + 1, 1).Value = list(list.count).Country
            .Cells(list.count + 1, 2).Value = list(list.count).Capital
            .Cells(list.count + 1, 3).Value = list(list.count).Area
            .Cells(list.count + 1, 4).Value = list(list.count).Language
            .Cells(list.count + 1, 5).Value = list(list.count).Money
            .Cells(list.count + 1, 6).Value = list(list.count).Population
            .Cells(list.count + 1, 7).Value = list(list.count).Government
            With .Range(Cells(list.count + 1, 1), Cells(list.count + 1, 7))
                .Borders.LineStyle = xlContinuous
                .Borders.Weight = xlThin
                .WrapText = False
                .Font.Size = 10
                .Font.Bold = False
                .Font.Name = "Arial Cyr"
                .HorizontalAlignment = xlCenter
                .VerticalAlignment = xlCenter
                .ShrinkToFit = False
            End With
            .Cells(list.count + 1, 3).HorizontalAlignment = xlRight
            .Cells(list.count + 1, 6).HorizontalAlignment = xlRight
            .Range(Cells(1, 1), Cells(1 + list.count, 7)).Columns.AutoFit
        End With
        
        Call SortFormAndSheet
        DataBase.Save
        Call RefillGovernmentList
        
        Dim index As Integer
        For index = 1 To list.count
            If list(index).Country = temp.Country Then
                Exit For
            End If
        Next index
        cboCountry.AddItem temp.Country, index - 1
        cboCountry.ListIndex = index - 1
        cboCountry_Change
        With lblError
            .Visible = True
            .Caption = "������ ������� ���������!"
            .ForeColor = &H8000&
        End With
    End If
    
    Exit Sub
    
Exception:
    MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbCritical + vbOKOnly, "����� ����"
    End
 
End Sub

Private Sub cmdEdit_Exit(ByVal Cancel As MSForms.ReturnBoolean)
  
    On Error GoTo Exception
    
    lblError.Visible = False
    
    Exit Sub
    
Exception:
    MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbCritical + vbOKOnly, "����� ����"
    End
    
End Sub

Private Sub cmdNext_Click()
  
    On Error GoTo Exception
    
    lblError.Visible = False
    If iterator = -1 And list.count <> 0 Then
        cboCountry.ListIndex = 0
        Exit Sub
    End If
    If iterator = list.count Then
        cboCountry.ListIndex = -1
        Exit Sub
    End If
    cboCountry.ListIndex = iterator
    
    Exit Sub
    
Exception:
    MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbCritical + vbOKOnly, "����� ����"
    End
 
End Sub

Private Sub cmdPrev_Click()
  
    On Error GoTo Exception
    
    lblError.Visible = False
    If iterator = -1 Then
        cboCountry.ListIndex = list.count - 1
        Exit Sub
    End If
    cboCountry.ListIndex = iterator - 2
    
    Exit Sub
    
Exception:
    MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbCritical + vbOKOnly, "����� ����"
    End
 
End Sub

Private Sub txtArea_Exit(ByVal Cancel As MSForms.ReturnBoolean)
  
    On Error GoTo Exception
    
    lblError.Visible = False
    
    Exit Sub
    
Exception:
    MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbCritical + vbOKOnly, "����� ����"
    End
 
End Sub

Private Sub txtArea_KeyPress(ByVal KeyAscii As MSForms.ReturnInteger)
  
    On Error GoTo Exception
    
    If Not (KeyAscii >= Asc("0") And KeyAscii <= Asc("9") Or KeyAscii = Asc(",") Or KeyAscii = Asc(".")) Then
        KeyAscii = 0
        With lblError
            .Caption = "� ��� ���� ����������� ���� ������ �������� ��������!"
            .Visible = True
            .ForeColor = &HFF&
        End With
    Else
        lblError.Visible = False
    End If
    
    Exit Sub
    
Exception:
    MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbCritical + vbOKOnly, "����� ����"
    End
      
End Sub

Private Sub txtPopulation_Exit(ByVal Cancel As MSForms.ReturnBoolean)
  
    On Error GoTo Exception
    
    lblError.Visible = False
    
    Exit Sub
    
Exception:
    MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbCritical + vbOKOnly, "����� ����"
    End
      
End Sub

Private Sub txtPopulation_KeyPress(ByVal KeyAscii As MSForms.ReturnInteger)

    On Error GoTo Exception
    
    If KeyAscii < Asc("0") Or KeyAscii > Asc("9") Then
        KeyAscii = 0
        With lblError
            .Caption = "� ��� ���� ����������� ���� ������ ������������� ��������!"
            .Visible = True
            .ForeColor = &HFF&
        End With
    Else
        lblError.Visible = False
    End If
                        
    Exit Sub
    
Exception:
    MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbCritical + vbOKOnly, "����� ����"
    End
    
End Sub

Private Sub UserForm_Activate()

    On Error GoTo Exception
    
    frmEdit.Caption = "�������������� ���� " & Chr(34) & Continent & Chr(34)
    lblError.Visible = False
    Call FillForm
    DataSheet.Columns("C:C").NumberFormat = "#,##0.0#"
    DataSheet.Columns("F:F").NumberFormat = 0
    DataBase.Save
    If list.count > 0 Then
        cmdEdit.Caption = "�������������"
        cmdDelete.Enabled = True
        Dim countriesList() As Variant
        ReDim countriesList(1 To list.count)
        Dim temp As Variant
        For i = 1 To list.count
            countriesList(i) = list(i).Country
        Next i
        cboCountry.list = countriesList
        cboCountry.ListIndex = 0
        iterator = 1
        Call RefillGovernmentList
    Else
        cmdEdit.Caption = "��������"
        cmdDelete.Enabled = False
        iterator = -1
    End If
    
    Exit Sub
    
Exception:
    MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbCritical + vbOKOnly, "����� ����"
    End
    
End Sub

Sub FillForm()
    
    On Error GoTo Exception
    
    Dim temp As Entry
    Dim count As Integer
    count = 2
    While DataSheet.Cells(count, 1) <> ""
        Set temp = New Entry
        temp.Country = DataSheet.Cells(count, 1)
        temp.Capital = DataSheet.Cells(count, 2)
        temp.Area = CDbl(DataSheet.Cells(count, 3))
        temp.Language = DataSheet.Cells(count, 4)
        temp.Money = DataSheet.Cells(count, 5)
        temp.Population = CLng(DataSheet.Cells(count, 6))
        temp.Government = DataSheet.Cells(count, 7)
        list.Add Item:=temp, Key:=temp.Country
        count = count + 1
    Wend
    
    Call SortFormAndSheet
    
    Exit Sub
    
Exception:
    Select Case Err.Number
    Case 13
        MsgBox "������ �� ����� � ��������� �������!", vbCritical + vbOKOnly, "����� ����"
        End
    Case Else
        MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbCritical + vbOKOnly, "����� ����"
        End
    End Select
    
End Sub


Sub SortFormAndSheet()

    On Error GoTo Exception
    
    Dim i, j As Integer
    Dim temp As Variant

    For i = 1 To list.count - 1
        For j = i + 1 To list.count
            If list(i).Country > list(j).Country Then
               Set temp = list(j)
               list.Remove j
               list.Add Item:=temp, Key:=temp.Country, Before:=i
            End If
        Next j
    Next i
    
    DataSheet.Range(Cells(2, 1), Cells(1 + list.count, 7)).SortSpecial SortMethod:=xlStroke, Key1:=Cells(2, 1), _
                                            Order1:=xlAscending, Header:=xlNo, Orientation:=xlSortColumns
    
    Exit Sub
    
Exception:
    MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbCritical + vbOKOnly, "����� ����"
    End
    
End Sub


Sub RefillGovernmentList()

    On Error GoTo Exception
    
    Set governmentList = New Collection
    For Each temp In list
        Call AddGovernment(temp.Government)
    Next
    cboGovernment.Clear
    For Each gov In governmentList
        cboGovernment.AddItem gov
    Next
                        
    Exit Sub
    
Exception:
    MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbCritical + vbOKOnly, "����� ����"
    End
    
End Sub


Sub AddGovernment(Government As String)

    On Error GoTo Exception
    
    For Each gov In governmentList
        If gov = Government Then
            Exit Sub
        End If
    Next
    governmentList.Add Government
    
    Dim i, j As Integer
    Dim temp As Variant
    
    For i = 1 To governmentList.count - 1
        For j = i + 1 To governmentList.count
            If governmentList(i) > governmentList(j) Then
                temp = governmentList(j)
                governmentList.Remove j
                governmentList.Add Item:=temp, Before:=i
            End If
        Next j
    Next i
                        
    Exit Sub
    
Exception:
    MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbCritical + vbOKOnly, "����� ����"
    End
    
End Sub



Function CheckFields() As Boolean

    On Error GoTo Exception
    
    CheckFields = True
    If Trim(cboCountry.Value) = "" Or Trim(txtCapital.Value) = "" Or Trim(txtArea.Value) = "" _
            Or Trim(txtLanguage.Value) = "" Or Trim(txtMoney.Value) = "" Or Trim(txtPopulation.Value) = "" _
            Or Trim(cboGovernment.Value) = "" Then
        CheckFields = False
        Exit Function
    End If
    If InStr(Trim(cboCountry.Value), "=") = 1 Or InStr(Trim(txtCapital.Value), "=") = 1 Or InStr(Trim(txtArea.Value), "=") = 1 Or _
            InStr(Trim(txtLanguage.Value), "=") = 1 Or InStr(Trim(txtMoney.Value), "=") = 1 Or _
            InStr(Trim(txtPopulation.Value), "=") = 1 Or InStr(Trim(cboGovernment.Value), "=") = 1 Then
        CheckFields = False
        Exit Function
    End If
    
    Dim dTest As Double
    Dim lTest As Long
    dTest = CDbl(Trim(Replace(txtArea.Value, ".", ",")))
    lTest = CLng(Trim(txtPopulation.Value))
                        
    Exit Function
    
Exception:
    Select Case Err.Number
    Case 13, 6
        CheckFields = False
        Exit Function
    Case Else
        MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbCritical + vbOKOnly, "����� ����"
        End
    End Select
      
End Function
