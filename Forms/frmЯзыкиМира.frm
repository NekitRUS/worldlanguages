VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} frm��������� 
   Caption         =   "����� ����"
   ClientHeight    =   4380
   ClientLeft      =   45
   ClientTop       =   375
   ClientWidth     =   7335
   OleObjectBlob   =   "frm���������.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "frm���������"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Base 0

Const Path As String = "C:\����������"
Dim sourceBook As Workbook '����� � ������� � ��������� ������������� ����������
Dim sourceSheet As Worksheet '���� � ��������� �������(������ ������ ��������� �� ������ �����)
Dim pathToWB As String '������ ���� �� ����� � ��������� �������
Dim myBook As Workbook '����� � ��������������� �������
Dim mySheet As Worksheet '���� � �������������� �����
Dim pathToFinalWB As String '������ ���� �� ����� � ��������������� �������
Dim languagesList() As String '��������� ������ ������, ������������� � ��������� ������������� �����
Dim listSize As Integer '���������� ��������� � ������� � �������
Dim selectedLanguage As String '�������� �����, ���������� �������������
Dim finalArray() As Variant '������ � ����������� �������� �� ���������� ��������(�����)


Private Sub cbo����_Change()

End Sub

Private Sub lblError_Click()

End Sub

'********************************************************************************************************
'���������� �������� �����.
'����� ��� ������ ������ ������, ������ "��" ����������.
'���������� ���������� ��������� ��������� ��� ������������� "������"(������ �� ������)
'********************************************************************************************************
Private Sub UserForm_Initialize()

    On Error GoTo Exception
    
    cmd��.Default = True
    cmd������.Cancel = True
    
    cbo����.Style = fmStyleDropDownList
    
    cmd��.Enabled = False
    cbo����.Visible = False
    lbl����.Visible = False
    lblError.Visible = False
    
    opt������.SetFocus
    opt������.Value = True
    opt������_Change
    opt���������.Value = False
    opt����.Value = False
    opt�������.Value = False
    opt������.Value = False
    
    Exit Sub
    
Exception:
    MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbCritical + vbOKOnly, "����� ����"
    Exit Sub
    
End Sub


'********************************************************************************************************
'���������� ��������� ��������� ������������� "���������".
'���� ������, �� ��������� ���� �� ��������������� �����.
'���� ����� ����������, ���������� ���������� ������ ������,
'����� ���������� ����� ��� ������.
'********************************************************************************************************
Private Sub opt���������_Change()

    On Error GoTo Exception
    
    If opt���������.Value = True Then
        pathToWB = Path & "\���������.xls"
        If BookCondition(pathToWB) <> -1 Then
            FillList pathToWB
        Else
            Set sourceBook = Nothing
            Set sourceSheet = Nothing
            cmd��.Enabled = False
            lblError.Visible = True
            lblError.Caption = "������!" & Chr(10) & "���� � ��������� ����������� �����������," & Chr(10) _
                       & "���������� ������� ������ ���������."
        End If
    End If
    
    Exit Sub
    
Exception:
    MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbCritical + vbOKOnly, "����� ����"
    Unload Me
    Exit Sub
    
End Sub


'********************************************************************************************************
'���������� ��������� ��������� ������������� "����".
'���� ������, �� ��������� ���� �� ��������������� �����.
'���� ����� ����������, ���������� ���������� ������ ������,
'����� ���������� ����� ��� ������.
'********************************************************************************************************
Private Sub opt����_Change()

    On Error GoTo Exception
    
    If opt����.Value = True Then
        pathToWB = Path & "\����.xls"
        If BookCondition(pathToWB) <> -1 Then
            FillList pathToWB
        Else
            Set sourceBook = Nothing
            Set sourceSheet = Nothing
            cmd��.Enabled = False
            lblError.Visible = True
            lblError.Caption = "������!" & Chr(10) & "���� � ��������� ����������� �����������," & Chr(10) _
                       & "���������� ������� ������ ���������."
        End If
    End If
        
    Exit Sub
    
Exception:
    MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbCritical + vbOKOnly, "����� ����"
    Unload Me
    Exit Sub
    
End Sub


'********************************************************************************************************
'���������� ��������� ��������� ������������� "�������".
'���� ������, �� ��������� ���� �� ��������������� �����.
'���� ����� ����������, ���������� ���������� ������ ������,
'����� ���������� ����� ��� ������.
'********************************************************************************************************
Private Sub opt�������_Change()

    On Error GoTo Exception
    
    If opt�������.Value = True Then
        pathToWB = Path & "\�������.xls"
        If BookCondition(pathToWB) <> -1 Then
            FillList pathToWB
        Else
            Set sourceBook = Nothing
            Set sourceSheet = Nothing
            cmd��.Enabled = False
            lblError.Visible = True
            lblError.Caption = "������!" & Chr(10) & "���� � ��������� ����������� �����������," & Chr(10) _
                       & "���������� ������� ������ ���������."
        End If
    End If
        
    Exit Sub
    
Exception:
    MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbCritical + vbOKOnly, "����� ����"
    Unload Me
    Exit Sub
    
End Sub


'********************************************************************************************************
'���������� ��������� ��������� ������������� "������".
'���� ������, �� ��������� ���� �� ��������������� �����.
'���� ����� ����������, ���������� ���������� ������ ������,
'����� ���������� ����� ��� ������.
'********************************************************************************************************
Private Sub opt������_Change()

    On Error GoTo Exception
    
    If opt������.Value = True Then
        pathToWB = Path & "\������.xls"
        If BookCondition(pathToWB) <> -1 Then
            FillList pathToWB
        Else
            Set sourceBook = Nothing
            Set sourceSheet = Nothing
            cmd��.Enabled = False
            lblError.Visible = True
            lblError.Caption = "������!" & Chr(10) & "���� � ��������� ����������� �����������," & Chr(10) _
                       & "���������� ������� ������ ���������."
        End If
    End If
            
    Exit Sub
    
Exception:
    MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbCritical + vbOKOnly, "����� ����"
    Unload Me
    Exit Sub
    
End Sub


'********************************************************************************************************
'���������� ��������� ��������� ������������� "������".
'���� ������, �� ��������� ���� �� ��������������� �����.
'���� ����� ����������, ���������� ���������� ������ ������,
'����� ���������� ����� ��� ������.
'********************************************************************************************************
Private Sub opt������_Change()

    On Error GoTo Exception
    
    If opt������.Value = True Then
        pathToWB = Path & "\������.xls"
        If BookCondition(pathToWB) <> -1 Then
            FillList pathToWB
        Else
            Set sourceBook = Nothing
            Set sourceSheet = Nothing
            cmd��.Enabled = False
            lblError.Visible = True
            lblError.Caption = "������!" & Chr(10) & "���� � ��������� ����������� �����������," & Chr(10) _
                       & "���������� ������� ������ ���������."
        End If
    End If
            
    Exit Sub
    
Exception:
    MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbCritical + vbOKOnly, "����� ����"
    Unload Me
    Exit Sub
    
End Sub


'********************************************************************************************************
'���������� ������� �� ������ "��".
'��������� ������������� �� ������ ���� ������������ � ��������������� ���������� � ����������� ��
'���������� ��������, ����������� � ����� �����/�����.
'���� �������� ���� ����������, ������������ ������������ ��� ��������. � ������ ��������������� ������
'���������� ������ ����� ����������� � ���������. � ��������� ������ ������������ ������������� � ���,
'��� ����� ������ ����� �� ��� ����������� � ��������� �����������.
'�� ��������� ������ ����������� ����������� ������ �� ����� � ��������� �������, ����� ���� ������
'��������������� � ���������� � ������, ���������� ��� �������� ������ � ����� �� ������������ ��������
'� �������� �����.
'���������� ��������� ������ ���������� ������ � �������������� �����.
'********************************************************************************************************
Private Sub cmd��_Click()

    On Error GoTo Exception

    '��������� ���� ������������ � ���������
    selectedLanguage = cbo����.Value
    
    '� ������ ��������� ������������ � ����� �����/����� ������� � ��������� ����,
    '������������ ������������� �� ����, � �������������� ���� �� ���������
    forbiddenChars = Array("~", "!", "@", "/", "\", "#", "$", "%", "^", "&", "*", "=", "|", "`", Chr(34))
    For i = LBound(forbiddenChars) To UBound(forbiddenChars)
        If InStr(selectedLanguage, forbiddenChars(i)) > 0 Then
            MsgBox "���������� ������� ���� ��� ����� � ����� ������.", vbCritical + vbOKOnly, "����� ����"
            Exit Sub
        End If
    Next i
    
    '����������� ��������� �������������� �����. � ������ � ������������� � ���������������
    '������ ������������ �� ������ � ������, ����� ���������
    pathToFinalWB = Path & "\" & selectedLanguage & "\" & selectedLanguage & ".xls"
    If BookCondition(pathToFinalWB) <> -1 Then
        Dim result As VbMsgBoxResult
        result = MsgBox("���� " & Chr(34) & selectedLanguage & ".xls" & Chr(34) & " ��� ����������." & Chr(10) & _
                "������ �������� ���?", vbQuestion + vbYesNo, "������ �����")
        If result = vbNo Then
            MsgBox "������ ����� �� �����������!", vbInformation + vbOKOnly, "����� ����"
            Unload Me
            Exit Sub
        Else
            For Each WB In Workbooks
                If WB.FullName = pathToFinalWB Then
                    WB.Close False
                    Exit For
                End If
            Next
            Kill pathToFinalWB
        End If
    End If
    
    '���������� ������ �� ����� � ��������� ������� � ������ � ���������
    If BookCondition(pathToWB) = 1 Then
        For Each WB In Workbooks
            If WB.FullName = pathToWB Then
                Set sourceBook = WB
                Set sourceSheet = sourceBook.Sheets(1)
                Exit For
            End If
        Next
    End If
    If sourceBook Is Nothing Then
        Set sourceBook = Workbooks.Open(pathToWB)
        Set sourceSheet = sourceBook.Sheets(1)
    End If
    
    '���� � ��������� ������� ����������� �� ������� ������ � ������, � ������ �� ����������� ��������
    '����������� �� ��������� �������
    Dim cellCount, finalCount As Integer
    cellCount = 2
    finalCount = 0
    Dim temp() As Variant
    Dim splited() As String
    While Trim(sourceSheet.Cells(cellCount, 1)) <> ""
        If Trim(sourceSheet.Cells(cellCount, 2)) = "" Or Not (IsNumeric(Trim(sourceSheet.Cells(cellCount, 3)))) _
        Or Trim(sourceSheet.Cells(cellCount, 4)) = "" Or Trim(sourceSheet.Cells(cellCount, 5)) = "" _
        Or Not (IsNumeric(Trim(sourceSheet.Cells(cellCount, 6)))) Or Trim(sourceSheet.Cells(cellCount, 7)) = "" Then
            MsgBox "������!" & Chr(10) & "��������� ������������ �������� ������!", vbCritical + vbOKOnly, "����� ����"
            Unload Me
            Exit Sub
        Else
            splited = Split(LCase(Trim(sourceSheet.Cells(cellCount, 4))), ", ")
            For i = LBound(splited) To UBound(splited)
                If splited(i) = selectedLanguage Then
                    ReDim Preserve temp(0 To 5, 0 To finalCount)
                    temp(0, finalCount) = Trim(sourceSheet.Cells(cellCount, 1))
                    temp(1, finalCount) = Trim(sourceSheet.Cells(cellCount, 2))
                    temp(2, finalCount) = Trim(sourceSheet.Cells(cellCount, 5))
                    temp(3, finalCount) = Val(Replace(Trim(sourceSheet.Cells(cellCount, 3)), ",", "."))
                    temp(4, finalCount) = Val(Replace(Trim(sourceSheet.Cells(cellCount, 6)), ",", "."))
                    temp(5, finalCount) = Trim(sourceSheet.Cells(cellCount, 7))
                    finalCount = finalCount + 1
                    Exit For
                End If
            Next i
        End If
        cellCount = cellCount + 1
    Wend
    
    '��������� ������ � ����������� ������� ��������������� ����� ����������� � ������
    '��� �������������� ������
    ReDim finalArray(0 To finalCount - 1, 0 To 5)
    For i = 0 To finalCount - 1
        finalArray(i, 0) = temp(0, i)
        finalArray(i, 1) = temp(1, i)
        finalArray(i, 2) = temp(2, i)
        finalArray(i, 3) = temp(3, i)
        finalArray(i, 4) = temp(4, i)
        finalArray(i, 5) = temp(5, i)
    Next i
    
    '���������� ��������� ������ ������ � �������������� �����
    Call WriteResults(finalCount)
                
    Exit Sub
    
Exception:
    Select Case Err.Number
    Case 52, 75 '(Bad file name or number, Path/File access error)
        MsgBox "���������� ��������� ���� � ����� ������!", vbCritical + vbOKOnly, "����� ����"
        Unload Me
        Exit Sub
    Case Else
        MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbCritical + vbOKOnly, "����� ����"
        Unload Me
        Exit Sub
    End Select
    
End Sub


'********************************************************************************************************
'���������� ������� �� ������ "������".
'����� ����������� �� ����������� ������.
'********************************************************************************************************
Private Sub cmd������_Click()

    On Error GoTo Exception
    
    If myBook Is Nothing Then
        Unload Me
    ElseIf BookCondition(pathToFinalWB) = -1 Then
        Application.DisplayAlerts = False
        myBook.Close False
        Application.DisplayAlerts = True
        Unload Me
    Else
        Unload Me
    End If
                
    Exit Sub
    
Exception:
    MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbCritical + vbOKOnly, "����� ����"
    Unload Me
    Exit Sub
    
End Sub


'********************************************************************************************************
'������� �������� ��������� ����� �� ����������� ����.
'������� ����������:
' -1 ���� �� ����������,
' 1  ���� �������,
' 0  ���� �������.
'********************************************************************************************************
Function BookCondition(fullPath As String) As Integer

   On Error GoTo Exception
    
    If Dir(fullPath) = "" Then
        BookCondition = -1
        Exit Function
    End If
    
    For Each WB In Workbooks
        If WB.FullName = fullPath Then
            BookCondition = 1
            Exit Function
        End If
    Next
    
    BookCondition = 0
                    
    Exit Function
    
Exception:
       Select Case Err.Number
    Case 52 '(Bad file name or number)
        Resume Next
    Case Else
        MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbCritical + vbOKOnly, "����� ����"
        Unload Me
        Exit Function
    End Select
    
End Function


'********************************************************************************************************
'��������� ���������� ���������� �������.
'********************************************************************************************************
Sub Sort(ByRef arr() As String)
    
    Dim i, j, imin As Integer
    Dim str As String
    
    On Error GoTo Exception
    
    For i = LBound(arr) To UBound(arr) - 1
        imin = i
        str = arr(i)
        For j = i To UBound(arr)
            If arr(j) < str Then
                str = arr(j)
                imin = j
            End If
        Next j
        Dim temp As String
        temp = arr(i)
        arr(i) = arr(imin)
        arr(imin) = temp
    Next i
                    
    Exit Sub
    
Exception:
    MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbCritical + vbOKOnly, "����� ����"
    Exit Sub
    
End Sub



'********************************************************************************************************
'��������� ���������� ����� � ������ � �������.
'���� ������ ��� �������� ���������� ����, ��������� �����������,
'����� ����������� ���������������� � ���� ����������� � ������.
'********************************************************************************************************
Sub AddLanguage(Language As String)

    On Error GoTo Exception
    
    For Each lang In languagesList
        If lang = Language Then
            Exit Sub
        End If
    Next
    ReDim Preserve languagesList(0 To listSize)
    languagesList(listSize) = Language
    listSize = listSize + 1
                        
    Exit Sub
    
Exception:
    MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbCritical + vbOKOnly, "����� ����"
    Exit Sub
    
End Sub


'********************************************************************************************************
'��������� ���������� ������ ������ �� �����.
'� ����� ����������� �������� ��������������� ������ � ���������� ��������� ���������� ����� � ������.
'���� �� ����� ������������ ������������ ������, ������ ������ ������ ������������ ����� ��� ������.
'********************************************************************************************************
Sub FillList(fullPath As String)

    Dim cellCount
    
    On Error GoTo Exception
    
    '���������� ������ �� ����� � ��������� ������� � ������ � ���������
    If BookCondition(fullPath) = 1 Then
        For Each WB In Workbooks
            If WB.FullName = fullPath Then
                Set sourceBook = WB
                Set sourceSheet = sourceBook.Sheets(1)
                Exit For
            End If
        Next
    End If
    If sourceBook Is Nothing Then
        Set sourceBook = Workbooks.Open(fullPath)
        Set sourceSheet = sourceBook.Sheets(1)
    End If
    sourceBook.Activate
    sourceSheet.Activate
    
    '���� � ������� ����������� �� ������� ������, � � ������ �� ����������,
    '������ ������� � ������� ����������� �� �������, � ��� ������� ����������� ��������
    '���������� ��������� ���������� �����.
    '� ������ ������ ������������ ����� ��� ������ � ��������� �����������.
    cellCount = 2
    listSize = 0
    ReDim languagesList(0 To 0)
    While Trim(sourceSheet.Cells(cellCount, 1)) <> ""
        If Trim(sourceSheet.Cells(cellCount, 2)) = "" Or Not (IsNumeric(Trim(sourceSheet.Cells(cellCount, 3)))) _
        Or Trim(sourceSheet.Cells(cellCount, 4)) = "" Or Trim(sourceSheet.Cells(cellCount, 5)) = "" _
        Or Not (IsNumeric(Trim(sourceSheet.Cells(cellCount, 6)))) Or Trim(sourceSheet.Cells(cellCount, 7)) = "" Then
            Set sourceBook = Nothing
            Set sourceSheet = Nothing
            cmd��.Enabled = False
            lblError.Visible = True
            lblError.Caption = "������!" & Chr(10) & "��������� ������������ �������� ������" & Chr(10) _
                                & "��� �������� ������ ���������."
            Exit Sub
        Else
            Dim temp() As String
            temp = Split(LCase(Trim(sourceSheet.Cells(cellCount, 4))), ", ")
            For i = LBound(temp) To UBound(temp)
                AddLanguage temp(i)
            Next i
            cellCount = cellCount + 1
        End If
    Wend
    
    '���� � ������� �� ����� ����������� ���������� ���������������� ���������������� �������,
    '� �� ��������� ���������� ������ �������
    Sort languagesList
    cbo����.list = languagesList
    cbo����.ListIndex = 0
    If cbo����.list(0) = "" Then
        Set sourceBook = Nothing
        Set sourceSheet = Nothing
        cmd��.Enabled = False
        lblError.Visible = True
        lblError.Caption = "������!" & Chr(10) & "��������� ������������ �������� ������" & Chr(10) _
                            & "��� �������� ������ ���������."
        Exit Sub
    End If
    
    '������ ������ ������������ �� �����, � ���������� �������� ������ "��"
    cbo����.Visible = True
    lbl����.Visible = True
    lblError.Visible = False
    cmd��.Enabled = True
    
    '������ �� ����� � ���� � ��������� ������� ������������
    Set sourceBook = Nothing
    Set sourceSheet = Nothing
                        
    Exit Sub
    
Exception:
    Select Case Err.Number
    Case 13 '(Type mismatch)
        Set sourceBook = Nothing
        Set sourceSheet = Nothing
        cmd��.Enabled = False
        lblError.Visible = True
        lblError.Caption = "������!" & Chr(10) & "��������� ������������ �������� ������" & Chr(10) _
                            & "��� �������� ������ ���������."
        Exit Sub
    Case Else
        MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbCritical + vbOKOnly, "����� ����"
        Unload Me
        Exit Sub
    End Select
    
End Sub


'********************************************************************************************************
'��������� ������ ���������� ������ � �������������� �����, ����������� ���������� ����� ������.
'��������� ����� ����� � ����� ������ � �� ���� ������������ �������������� ������, � ����� �����������.
'����� ������������� ���, ��������������� ���������� ������������� ����������.
'���� �������������, � �������������� ����� ����������� ��� ������, ��������������� ���������� �����.
'********************************************************************************************************
Sub WriteResults(count As Integer)

    On Error GoTo Exception
    
    '���������� ��� �������������� ����� ������������� ����� ��������� ����� � ����� ������, ��������
    '�������� ��������� � ��������� ���������� ����������, ���������� ��� ����� � ��������������� �������
    '������������� ������ ���� �����
    Dim sheetsCount As Integer
    sheetsCount = Application.SheetsInNewWorkbook
    Application.SheetsInNewWorkbook = 1
    Set myBook = Workbooks.Add
    Application.SheetsInNewWorkbook = sheetsCount
    Set mySheet = myBook.Sheets(1)
    mySheet.Name = Left(sourceBook.Name, Len(sourceBook.Name) - 4)
    
    '���������� ������ �� ��������������� ������� ����������� �� ���� � �����������
    mySheet.Cells(3, 1).Resize(count, 6) = finalArray
    mySheet.Range(Cells(3, 1), Cells(2 + count, 6)).SortSpecial SortMethod:=xlStroke, Key1:=Cells(3, 1), _
                                            Order1:=xlAscending, Header:=xlNo, Orientation:=xlSortColumns
    
    '������������� ����� �����
    mySheet.Cells(1, 1).Value = "���������: " & Left(sourceBook.Name, Len(sourceBook.Name) - 4)
    mySheet.Range("A1:C1").Merge
    mySheet.Cells(1, 4).Value = "����������� ����: " & selectedLanguage
    mySheet.Range("D1:F1").Merge
    With mySheet.Range("A1:F1")
        .WrapText = False
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlCenter
        .ShrinkToFit = True
        .Font.Size = 18
        .Font.Bold = True
        .Interior.ColorIndex = 45
    End With
    mySheet.Cells(2, 1).Value = "������"
    mySheet.Cells(2, 2).Value = "�������"
    mySheet.Cells(2, 3).Value = "�������� �������"
    mySheet.Cells(2, 4).Value = "�������, ��. ��"
    mySheet.Cells(2, 5).Value = "���������, ���"
    mySheet.Cells(2, 6).Value = "����� ���������"
    With mySheet.Range("A2:F2")
        .WrapText = False
        .HorizontalAlignment = xlCenter
        .VerticalAlignment = xlCenter
        .Font.Size = 12
        .Font.Bold = True
        .Interior.ColorIndex = 40
    End With
    
    '������������� ������� � ����������� �������
    mySheet.Range(Cells(3, 4), Cells(2 + count, 4)).NumberFormat = "General"
    mySheet.Range(Cells(3, 5), Cells(2 + count, 5)).NumberFormat = "#,##0"
    mySheet.Range(Cells(1, 1), Cells(1, 6)).Borders(xlInsideVertical).LineStyle = xlNone
    mySheet.Range(Cells(3, 1), Cells(2 + count, 6)).Borders.LineStyle = xlContinuous
    mySheet.Range(Cells(3, 1), Cells(2 + count, 6)).Borders.Weight = xlThin
    mySheet.Range(Cells(1, 1), Cells(2, 6)).Borders.Weight = xlMedium
    mySheet.Range(Cells(1, 1), Cells(1, 6)).Borders(xlInsideVertical).LineStyle = xlNone
    mySheet.Range(Cells(1, 1), Cells(2 + count, 6)).Columns.AutoFit
    
    '��������� ����� ��� �������������� �����
    If Dir(Path & "\" & selectedLanguage, vbDirectory) = "" Then
        MkDir (Path & "\" & selectedLanguage)
    End If
    
    '����������� �������������� �����
    Application.DisplayAlerts = False
    myBook.SaveAs pathToFinalWB, xlWorkbookNormal
    Application.DisplayAlerts = True
    
    '������������ ���� � ��������������� �������
    myBook.Activate
    mySheet.Activate
    
    '������������ ������������� �� �������� ������������ ������ �����, � ��������� �����������
    MsgBox "������ ����� ������� �����������!", vbInformation + vbOKOnly, "����� ����"
    Unload Me
                            
    Exit Sub
    
Exception:
    Select Case Err.Number
    Case 52, 76 '(Bad file name or number, Path not found)
        MsgBox "���������� ��������� ���� � ����� ������.", vbCritical + vbOKOnly, "����� ����"
        If myBook Is Nothing Then
            Unload Me
        Else
            Application.DisplayAlerts = False
            myBook.Close False
            Application.DisplayAlerts = True
            Unload Me
        End If
        Exit Sub
    Case Else
        MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbCritical + vbOKOnly, "����� ����"
        Unload Me
        Exit Sub
    End Select
    
End Sub
