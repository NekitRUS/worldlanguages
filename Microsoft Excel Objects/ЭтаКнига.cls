VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "��������"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = True
'********************************************************************************************************
'���������� �������� ����� � ����������.
'�� ����������� ������ ������������ ����������� ���� ��� ������ � ����������.
'********************************************************************************************************
Private Sub Workbook_Open()

    Dim myMenu As CommandBarPopup
    Dim runButton, aboutButon As CommandBarButton
    
    On Error GoTo Exception
    
    Set myMenu = Application.CommandBars("Worksheet Menu Bar").Controls.Add(msoControlPopup)
    myMenu.Caption = "������ � ����������"
    
    Set runButton = myMenu.Controls.Add(msoControlButton)
    With runButton
        .Caption = "����������� ����"
        .Style = msoButtonCaption
        .OnAction = "ShowForm"
    End With
    
    Set editButon = myMenu.Controls.Add(msoControlButton)
    With editButon
        .Caption = "����/�������������� ��"
        .Style = msoButtonCaption
        .OnAction = "ShowEdit"
    End With
    
    Set createButon = myMenu.Controls.Add(msoControlButton)
    With createButon
        .Caption = "�������� ����� ��"
        .Style = msoButtonCaption
        .OnAction = "ShowCreate"
    End With
    
    Set aboutButon = myMenu.Controls.Add(msoControlButton)
    With aboutButon
        .Caption = "� ���������"
        .Style = msoButtonCaption
        .OnAction = "ShowAbout"
    End With
    
    Exit Sub
    
Exception:
    MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbCritical + vbOKOnly, "����� ����"
    Exit Sub

End Sub


'********************************************************************************************************
'���������� �������� ����� � ����������.
'��������� ���� ��� ������ � ����������.
'********************************************************************************************************
Private Sub Workbook_BeforeClose(Cancel As Boolean)

    On Error GoTo Exception
    
    For Each myMenu In Application.CommandBars("Worksheet Menu Bar").Controls
        If myMenu.Caption = "������ � ����������" Then
            myMenu.Delete
            Exit For
        End If
    Next
    Exit Sub
    
Exception:
    MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbCritical + vbOKOnly, "����� ����"
    Exit Sub

End Sub
