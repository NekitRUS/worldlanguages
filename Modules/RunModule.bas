Attribute VB_Name = "RunModule"
'********************************************************************************************************
'��������� ������� ����� � ����������� �� ������.
'********************************************************************************************************
Sub ShowAbout()

    On Error GoTo Exception
    
    frmAbout.Show
    
    Exit Sub
    
Exception:
    MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbCritical + vbOKOnly, "����� ����"
    Exit Sub
    
End Sub


'********************************************************************************************************
'��������� ������� ������� �����.
'********************************************************************************************************
Sub ShowForm()

    On Error GoTo Exception
    
    frm���������.Show
    
    Exit Sub
    
Exception:
    MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbCritical + vbOKOnly, "����� ����"
    Exit Sub

End Sub


'********************************************************************************************************
'��������� ������� ����� �� ��������������.
'********************************************************************************************************
Sub ShowEdit()

    On Error GoTo Exception
    
    Dim pathToOpen As Variant
    pathToOpen = Application.GetOpenFilename(FileFilter:="����� Excel (*.xls),*.xls", _
                Title:="�������� ���� ����������", MultiSelect:=False)
    If pathToOpen = False Then
        End
    ElseIf Right(pathToOpen, 4) <> ".xls" Then
        MsgBox "���������� ������� ���� ������� ����!", vbExclamation + vbOKOnly, "����� ����"
        End
    ElseIf pathToOpen = ThisWorkbook.FullName Then
        MsgBox "���������� ������� ���� � ����������!", vbExclamation + vbOKOnly, "����� ����"
        End
    Else
        frmEdit.Path = pathToOpen
        frmEdit.Continent = Right(pathToOpen, Len(pathToOpen) - InStrRev(pathToOpen, "\"))
        
        For Each WB In Workbooks
            If WB.FullName = pathToOpen Then
                Set frmEdit.DataBase = WB
                WB.Activate
                Set frmEdit.DataSheet = WB.Sheets(1)
                WB.Sheets(1).Activate
            End If
        Next
        For Each WB In Workbooks
            If WB.Name = frmEdit.Continent And WB.FullName <> pathToOpen Then
                WB.Close False
            End If
        Next
        If frmEdit.DataBase Is Nothing Then
            Set frmEdit.DataBase = Workbooks.Open(pathToOpen)
            Set frmEdit.DataSheet = frmEdit.DataBase.Sheets(1)
        End If
        If frmEdit.DataSheet.Cells(1, 1).Value <> "������" Or _
                frmEdit.DataSheet.Cells(1, 2).Value <> "�������" Or _
                frmEdit.DataSheet.Cells(1, 3).Value <> "�������, ��. ��" Or _
                frmEdit.DataSheet.Cells(1, 4).Value <> "����������� ��." Or _
                frmEdit.DataSheet.Cells(1, 5).Value <> "�������� �������" Or _
                frmEdit.DataSheet.Cells(1, 6).Value <> "���������, ���." Or _
                frmEdit.DataSheet.Cells(1, 7).Value <> "����� ���������" Then
            MsgBox "�������� ������ �������� ������ � ����!", vbExclamation + vbOKOnly, "����� ����"
            End
        End If
        frmEdit.Show
    End If
    
    Exit Sub
    
Exception:
    MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbCritical + vbOKOnly, "����� ����"
    Exit Sub
    
End Sub


'********************************************************************************************************
'��������� ������� ����� �� ��������.
'********************************************************************************************************
Sub ShowCreate()

    On Error GoTo Exception
    
    Dim pathToCreate As Variant
    pathToCreate = Application.GetSaveAsFilename(InitialFileName:="", _
                    FileFilter:="����� Excel (*.xls),*.xls", _
                    Title:="�������� ���� � ��� ����� ��")
    If pathToCreate = False Then
        End
    ElseIf Right(pathToCreate, 4) <> ".xls" Then
        MsgBox "���������� ��������� ���� ������� ����!", vbExclamation + vbOKOnly, "����� ����"
        End
    ElseIf pathToCreate = ThisWorkbook.FullName Then
        MsgBox "���������� �������� ���� � ����������!", vbExclamation + vbOKOnly, "����� ����"
        End
    Else
        frmEdit.Path = pathToCreate
        frmEdit.Continent = Right(pathToCreate, Len(pathToCreate) - InStrRev(pathToCreate, "\"))
    
        If Dir(pathToCreate) <> "" Then
            Dim result As VbMsgBoxResult
            result = MsgBox("���� � ����� ������ ��� ����������." & Chr(13) _
                        & "������ ���������� ����?", vbYesNo + vbQuestion, "����� ����")
            If result <> vbYes Then
                End
            Else
                For Each WB In Workbooks
                    If WB.FullName = pathToCreate Then
                        WB.Close False
                    End If
                Next
                Kill pathToCreate
            End If
        End If
        For Each WB In Workbooks
            If WB.Name = frmEdit.Continent Then
                WB.Close False
            End If
        Next
        
        Dim count As Integer
        count = Application.SheetsInNewWorkbook
        Application.SheetsInNewWorkbook = 1
        Set frmEdit.DataBase = Workbooks.Add
        Set frmEdit.DataSheet = frmEdit.DataBase.Sheets(1)
        
        frmEdit.DataSheet.Cells(1, 1).Value = "������"
        frmEdit.DataSheet.Cells(1, 2).Value = "�������"
        frmEdit.DataSheet.Cells(1, 3).Value = "�������, ��. ��"
        frmEdit.DataSheet.Cells(1, 4).Value = "����������� ��."
        frmEdit.DataSheet.Cells(1, 5).Value = "�������� �������"
        frmEdit.DataSheet.Cells(1, 6).Value = "���������, ���."
        frmEdit.DataSheet.Cells(1, 7).Value = "����� ���������"
        With frmEdit.DataSheet.Range("A1:G1")
            .Borders.LineStyle = xlContinuous
            .Borders.Weight = xlThin
            .WrapText = False
            .Font.Size = 10
            .Font.Bold = True
            .Font.Name = "Arial Cyr"
            .HorizontalAlignment = xlCenter
            .VerticalAlignment = xlCenter
            .ShrinkToFit = False
            .Columns.AutoFit
        End With
        
        frmEdit.DataBase.SaveAs Filename:=pathToCreate, FileFormat:=xlWorkbookNormal
        Application.SheetsInNewWorkbook = count
        frmEdit.cmdEdit.Caption = "��������"
        frmEdit.Show
    End If
    
    Exit Sub
    
Exception:
    MsgBox "�������������� ������!" & Chr(10) & Err.Description, vbCritical + vbOKOnly, "����� ����"
    Exit Sub
    
End Sub
